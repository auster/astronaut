# syntax=docker/dockerfile:1
FROM eclipse-temurin:18
COPY target /app
CMD ["java","-cp", "app/astronaut.jar:app/lib/*:.", "io.github.codedauster.internal.Main"]
