package io.github.codedauster.data;

import com.mongodb.client.MongoClients;
import dev.morphia.Datastore;
import dev.morphia.Morphia;

import javax.inject.Inject;
import javax.inject.Named;

public class MongoDataSource implements IDataSource{
    @Inject
    @Named("MONGO_PASSWORD")
    private String pw;
    @Inject
    @Named("MONGO_USERNAME")
    private String user;
    private final Datastore datastore;
    public MongoDataSource() {
         datastore = Morphia.createDatastore(MongoClients.create("mongodb://_USER:_PW@mongo/?authSource=admin".replaceAll("_USER",user).replaceAll("_PW", pw)), "astronaut");
         datastore.getMapper().mapPackage("io.github.codedauster");

    }
}
