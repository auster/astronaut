package io.github.codedauster;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoClients;
import dev.morphia.Datastore;
import dev.morphia.Morphia;
import io.github.codedauster.commands.AbstractCommand;
import io.github.codedauster.commands.CommandListener;
import io.github.codedauster.commands.ICommandHandler;
import io.github.codedauster.listeners.RegisterSlashCommands;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.inject.Inject;
import javax.inject.Named;
import javax.security.auth.login.LoginException;
import java.util.List;

public class Astronaut {
    @Inject
    private ICommandHandler handler;
    @Inject
    private CommandListener commandListener;
    @Inject
    private RegisterSlashCommands registerSlashCommands;

    public Astronaut() {

    }

    @Inject
    public void registerCommands(@Named("commands") List<AbstractCommand> commands) {
        commands.forEach(handler::addCommand);
    }

    public void build(String token) {
        JDABuilder builder = JDABuilder.createDefault(token);
        builder.enableIntents(GatewayIntent.MESSAGE_CONTENT);
        builder.setBulkDeleteSplittingEnabled(false);
        builder.setActivity(Activity.listening("everybody! jk fuck u all"));

        builder.addEventListeners(commandListener, registerSlashCommands);


        try {

            builder.build();
        } catch (LoginException e) {
            throw new RuntimeException(e);
        }
    }


}
