package io.github.codedauster.internal;

import com.google.inject.Guice;
import com.google.inject.Injector;
import io.github.codedauster.Astronaut;

import javax.inject.Inject;


public class Main {
    @Inject
    private Astronaut astronaut;

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new MainModule(), new CommandsModule());
        var main = new Main();
        injector.injectMembers(main);
        main.run(args.length == 0 ? System.getenv("TOKEN") : args[0]);
    }

    public Main run(String args) {
        astronaut.build(args);
        return this;
    }


}
