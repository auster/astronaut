package io.github.codedauster.internal;

import com.google.inject.AbstractModule;
import com.google.inject.TypeLiteral;
import com.google.inject.name.Names;
import io.github.classgraph.ClassGraph;
import io.github.classgraph.ScanResult;
import io.github.codedauster.commands.AbstractCommand;
import io.github.codedauster.commands.Command;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

public class CommandsModule extends AbstractModule {
    @Override
    protected void configure() {
        try (ScanResult scanResult = new ClassGraph().acceptPackages("io.github.codedauster.commands.impl")
                .enableClassInfo().scan()) {
            List<Class<AbstractCommand>> result = scanResult
                    .getSubclasses(AbstractCommand.class.getName())
                    .loadClasses(AbstractCommand.class);
            var res = result.stream().map(x -> {
                try {
                    return x.getDeclaredConstructor(null).newInstance();
                } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
                    throw new RuntimeException(e);
                }
            }).toList();
            res.forEach(s -> {
                String name = s.getClass().getAnnotation(Command.class).value();
                bind(AbstractCommand.class).annotatedWith(Names.named(name)).toInstance(s);
            });
            bind(new TypeLiteral<List<AbstractCommand>>() {
            }).annotatedWith(Names.named("commands")).toInstance(res);
        }

    }

}
