package io.github.codedauster.internal;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import io.github.codedauster.Astronaut;
import io.github.codedauster.commands.CommandHandler;
import io.github.codedauster.commands.CommandListener;
import io.github.codedauster.commands.ICommandHandler;
import io.github.codedauster.config.ConfigHandler;
import io.github.codedauster.config.IConfigHandler;
import io.github.codedauster.listeners.RegisterSlashCommands;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MainModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(String.class).annotatedWith(Names.named("MONGO_PASSWORD")).toInstance(System.getenv("MONGO_INITDB_ROOT_PASSWORD"));
        bind(String.class).annotatedWith(Names.named("MONGO_USERNAME")).toInstance(System.getenv("MONGO_INITDB_ROOT_USERNAME"));

        bind(Astronaut.class).toInstance(new Astronaut());
        bind(ICommandHandler.class).to(CommandHandler.class).asEagerSingleton();
        bind(IConfigHandler.class).to(ConfigHandler.class);
        bind(CommandListener.class).toInstance(new CommandListener());
        bind(RegisterSlashCommands.class).toInstance(new RegisterSlashCommands());
        bind(String.class).annotatedWith(Names.named("prefix")).toInstance("_");
        bind(Logger.class).toInstance(LoggerFactory.getLogger(Astronaut.class));
    }
}
