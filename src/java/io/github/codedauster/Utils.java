package io.github.codedauster;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Utils {
    public static long getIdFromMention(String mention) {
        return Long.parseLong(getIdFromMentionAsString(mention));
    }

    public static String getIdFromMentionAsString(String mention) {
        return mention.substring(2, mention.length() - 1);
    }

    public static String getStringFromArray(String[] args, int start, int end) {
        return Arrays.stream(Arrays.copyOfRange(args, start, end)).map(s -> s + " ").collect(Collectors.joining()).trim();

    }

    public static String getStringFromArray(String[] args, int start) {
        return getStringFromArray(args, start, args.length);
    }
}
