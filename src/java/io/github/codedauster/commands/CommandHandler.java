package io.github.codedauster.commands;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CommandHandler implements ICommandHandler {

    private final Map<String, AbstractCommand> commandMap = new HashMap<>(10);

    public void addCommand(AbstractCommand abstractCommand) {
        String command = abstractCommand.getClass().getAnnotation(Command.class).value();
        commandMap.put(command, abstractCommand);
    }

    public void addCommands(AbstractCommand... abstractCommands) {
        Arrays.stream(abstractCommands).forEach(this::addCommand);
    }

    public Map<String, AbstractCommand> getCommandMap() {
        return commandMap;
    }
}
