package io.github.codedauster.commands;

import java.util.Map;

public interface ICommandHandler {
    void addCommands(AbstractCommand... abstractCommands);

    void addCommand(AbstractCommand abstractCommand);

    Map<String, AbstractCommand> getCommandMap();
}
