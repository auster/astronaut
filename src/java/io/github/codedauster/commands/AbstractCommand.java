package io.github.codedauster.commands;

import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.slf4j.Logger;

import javax.inject.Inject;

public abstract class AbstractCommand {
    @Inject
    protected Logger logger;

    public abstract SlashCommandData generateData(SlashCommandData data);

    public abstract void execute(SlashCommandInteractionEvent event);
}
