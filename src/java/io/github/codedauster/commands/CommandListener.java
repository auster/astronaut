package io.github.codedauster.commands;

import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import javax.inject.Inject;

public class CommandListener implements EventListener {
    @Inject
    Logger logger;
    @Inject
    private ICommandHandler handler;

    @Override
    public void onEvent(@NotNull GenericEvent genericEvent) {
        if (genericEvent instanceof SlashCommandInteractionEvent event) {
            AbstractCommand cmd = handler.getCommandMap().get(event.getName());
            if (cmd == null) return;
            cmd.execute(event);
        }

    }

}
