package io.github.codedauster.commands.impl;

import io.github.codedauster.commands.AbstractCommand;
import io.github.codedauster.commands.Command;
import io.github.codedauster.commands.Description;
import net.dv8tion.jda.api.events.interaction.command.SlashCommandInteractionEvent;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.SlashCommandData;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.concurrent.atomic.AtomicLong;

@Command("say")
@Description("Echo")
public class Say extends AbstractCommand {
    @Inject
    Logger logger;

    @Override
    public SlashCommandData generateData(SlashCommandData data) {
        data.addOption(OptionType.STRING, "message", "Message to be sent", true, false);
        data.addOption(OptionType.CHANNEL, "channel", "Channel for the message to be sent", false, false);
        return data;
    }

    @Override
    public void execute(SlashCommandInteractionEvent event) {
        event.deferReply(true).queue();
        AtomicLong id = new AtomicLong(0L);
        event.getChannel().sendMessage(event.getOption("message").getAsString()).queue(s -> {
            id.set(s.getIdLong());
        });
        event.getHook().editOriginal("Message sent!").queue();

    }
}
