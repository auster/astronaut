package io.github.codedauster.listeners;

import com.google.inject.name.Named;
import io.github.codedauster.commands.AbstractCommand;
import io.github.codedauster.commands.Description;
import io.github.codedauster.commands.ICommandHandler;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.events.ReadyEvent;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.interactions.commands.build.Commands;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.Map;

public class RegisterSlashCommands implements EventListener {
    @Inject
    private ICommandHandler handler;


    @Override
    public void onEvent(@NotNull GenericEvent event) {
        //If Bot Joins Guild
        if (event instanceof GuildJoinEvent e) {
            e.getJDA().getGuilds().forEach(s -> s.retrieveCommands().queue(c -> {

            }));
        } else if (event instanceof ReadyEvent e) {

            //TODO: if not registered, register
            e.getJDA().getGuilds().forEach(this::addCommandsToGuild);

        }
    }

    public void addCommandsToGuild(Guild guild) {
        for (Map.Entry<String, AbstractCommand> entry : handler.getCommandMap().entrySet()) {
            String command = entry.getKey();
            AbstractCommand abstractCommand = entry.getValue();
            String desc = abstractCommand.getClass().getAnnotation(Description.class).value();
            guild.upsertCommand(abstractCommand.generateData(Commands.slash(command, desc).setGuildOnly(true))).queue();

        }
    }
}
